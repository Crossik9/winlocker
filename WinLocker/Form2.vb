﻿Public Class Form2

    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If My.User.IsInRole(ApplicationServices.BuiltInRole.Administrator) Then
            Form1.Show()
            Me.Close()
        Else
            MsgBox("Uruchom Jako Administrator!")
            Me.Close()
        End If
    End Sub
End Class